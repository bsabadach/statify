/* jshint strict: true */
/* global Backbone,_,statify */
(function (root, _, Backbone, statify) {

	'use strict';


	statify.utils = {
		$: Backbone.$,
		_: _
	};



	var makeAsStatesClient = function (View) {
		var StatesClient = statify.StatesClient,
			// enhance Bakbone view with the StatesClient behaviour
			StateClientView = View.extend(StatesClient);

		// the render method must be overridden: view states are defined by querying the content of a DOM element attached to view.
		// If a view is rendered via a template engine, states must be defined after the render phase.


		StateClientView.prototype.render = (function () {

			var memo = View.prototype.render;
			
			return function () {
				var rendered = memo.call(this);
				this.initializeStates();
				return rendered || this.$el.html();
			};
		}());

		return StateClientView;

	};

	// define and add the StatesClientView to the root of the Backbone object
	Backbone.StatesClientView = makeAsStatesClient(Backbone.View);

	// add the makeAsStatesClient as a static method of the Backbone.View class
	Backbone.View.makeAsStatesClient = makeAsStatesClient;



})(this, _, Backbone, statify);