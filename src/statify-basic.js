/**
 * @license Copyright (c) 2013 Boris Sabadach
 * LICENSE: see the license.txt file. If file is missing, this file is subject
 * to the MIT License at: http://www.opensource.org/licenses/mit-license.php.
 */

/**
 * statify.js
 *
 * @version 0.1.0
 * @author boris Sabadach
 * boris@washmatique.fr
 */
/* jshint strict: true */
var statify = (function (root, undefined) {

	"use strict";

	var document = root.document,
		console = root.console,
		CSSStyleRule = root.CSSStyleRule,
		CSSRule = root.CSSRule,
		_core = {};

	//---------------------------------
	// State events types
	//-----------------------------------
	_core.EXIT = "state:exit";
	_core.EXITED = "state:exited";
	_core.ENTER = "state:enter";
	_core.CHANGED = "state:changed";


	_core.config = {
		stateAttr: "data-states",
		inAttr: "data-states-inc",
		outAttr: "data-states-exc",
		cssModifier: "-",
		triggerFunction: "trigger"
	};

	var _notImplemented = function (func) {
		_debug(func + 'is not loaded or implemented');
	};


	_core.utils = {
		$: function(){
			console.log(arguments);
		},
		_: function () {
			_notImplemented("_");
			
			return {};
		}
	};


	// turn off debug by default
	_core.DEBUG = true;



	/**
	 * add the trim function if not availiable
	 * @type {*}
	 */
	if (typeof String.prototype.trim !== "function") {
		String.prototype.trim = _core.utils._.trim || _core.utils.$.trim;
	}


	var _debug = (function () {
		var join = [].join,
			canLog = console && console.log;
		return function () {
			return canLog && _core.DEBUG && console.log(join.call(arguments, ','));
		};
	}());


	/**
	 * CSS utility functions.
	 * @type {*}
	 */
	_core.CSS = (function (document, core) {
		var _hasSheets = true,
			_stylesCache = {},
			_acceptedMediaTypes = ["screen", "all", ""],
			utils = core.utils;

		return {

			/**
			 * return true if among the comma separated string list of media types declared on a sheet and passed as parameter
			 * should be accepted for further process
			 * @param mediaTypes
			 * @return {Boolean}
			 * @private
			 */
			_acceptSheet: function (sheet) {
				var mediaTypes = sheet.media.mediaText;
				mediaTypes = mediaTypes.split(",");
				return utils._.find(mediaTypes, function (mediaType) {
					return utils._.indexOf(_acceptedMediaTypes, mediaType.trim()) >= 0;
				}) !== void 0;
			},

			/**
			 *  returns true if in the rule.selectorText passed as parameter a rule matches
			 *  the selectorText passed as parameter.Strict equallity is founded. All descendant rules
			 * @param rule
			 * @param selectorText
			 * @return {*}
			 * @private
			 */
			_ruleHasSelector: function (rule, selectorText) {
				var selectors = rule.selectorText.split(",");
				return !!utils._.find(selectors, function (aSelectorText) {
					return aSelectorText.trim() === selectorText;
				});
			},

			/**
			 * return the Style object among rules passed as parameter if one of the rules selectorText contains
			 * the selector string passed a parameter.
			 * @param rules a list of Rules
			 * @param selector : a style selector
			 * @return {*}
			 * @private
			 */
			_findStyleInRules: function (rules, selector) {
				var self = this;
				var rule = utils._.find(rules, function (rule) {
					//in Opera rule.constructor returns the interface name and not the implementation
					return (rule.constructor === CSSStyleRule || rule.constructor === CSSRule) && self._ruleHasSelector(rule, selector);
				});
				return rule === void 0 ? null : rule.style;
			},


			/**
			 * returns the Style Object and caches it if a CSS rule was found among all
			 * declared style sheets in the document.Sheets loaded from a different domain are excluded
			 * @param selector
			 * @return {*}
			 */
			findStyle: function (selector) {
				if (_hasSheets && _stylesCache[selector] !== void 0) return _stylesCache[selector];
				var sheets = document.styleSheets;
				_hasSheets = sheets === void 0;
				if (_hasSheets) return null;
				var sheet,
					rules,
					styleRule;
				for (var i = 0, l = sheets.length; i < l; i++) {
					try {
						sheet = sheets[i];
						if (!sheet || !this._acceptSheet(sheet)) continue; // exclude all media type except screen
						rules = sheet.rules || sheet.cssRules;
						if (rules === null) continue;
						styleRule = this._findStyleInRules(rules, selector);
						if (styleRule !== null) {
							_stylesCache[selector] = styleRule;
							return styleRule;
						}
					} catch (e) {
						console.log(e.message);
						if (e.name === 'SecurityError') continue;
						else throw e;
					}
				}
				_stylesCache[selector] = "empty";
				return _stylesCache[selector];
			},


			/**
			 * returns true if a CSS rule corresponding to the selector rule was found among
			 * the matching style sheets
			 * @param selector
			 */
			hasRule: function (selector) {
				return this.findStyle(selector) !== "empty";
			}

		};

	}(document, _core));


	//---------
	// Core
	//---------
	/**
	 * The ViewStateElement object wraps a DOM element (queried by the DOM selector library) that has a role in a view state.
	 * The class provides methods to manage the element visibility:display:block/none or visibility:visible/hidden
	 * according the keepLayout option in the constructor
	 * @type {*}
	 */
	(function (core) {

		var _CSS = core.CSS;
		var _config = core.config;

		var _displayPolicies = [
			{
				type: "display",
				yes: "block",
				no: "none"
            },
			{
				type: "visibility",
				yes: "visible",
				no: "hidden"
            }

        ];

		var ViewStateElement = function ($el, keepLayout) {
			this.$el = $el;
			this.display = _displayPolicies[keepLayout ? 1 : 0];
			this.ensure();
		};

		ViewStateElement.prototype = {
			ensure: function () {
				if (this.$el.attr("class") === void 0) throw new Error(this.$el.selector + " element(s) must have at least a base style class");
				this.baseStyle = this.$el.attr("class").split(" ").pop();
			},

			isDisplayed: function () {
				// its' simpler to compare to the not displayed css value because in the case of display block/none
				// the initial display value of the element could not be "block"  but "inline-block" or else
				return this.$el.css(this.display.type) !== this.display.no;
			},

			toggle: function () {
				this.$el.css(this.display.type, this.isDisplayed() ? this.display.no : this.display.yes);
			},

			removeStateStyle: function (state) {
				var style = this.baseStyle + _config.cssModifier + state;
				if (style && this.$el.hasClass(style)) this.$el.removeClass(style);
			},

			addStateStyle: function (state) {
				var selector = this.getStyleSelector(state);
				if (_CSS.hasRule(selector)) this.$el.addClass(selector.replace(".", ""));
			},

			getStyleSelector: function (state) {
				return "." + this.baseStyle + _config.cssModifier + state;
			}

		};
		core.ViewStateElement = ViewStateElement;

	}(_core));


	/**
	 *  The ViewState class encapsulate the different states declared on a DOM element.
	 * - it references arrays of child DOM elements (state targets) that are included or excluded from the state
	 * - the class provide an 'enter' and an 'exit' method that manages the state targets lifecycle during states transitions
	 * @type {*}
	 */
	(function (core) {

		var ViewStateElement = core.ViewStateElement;
		var _utils = core.utils;
		/**
		 * @param name : the state name
		 * @param $el : the DOM element targeted as a state container
		 * @param keepLayout: the display policy for state targets.
		 * @constructor
		 */
		var ViewState = function (name, $el, keepLayout) {
			this.$el = $el;
			this.name = name;
			this.keepLayout = keepLayout;
			this.transDuration = -1;
			this.inclusions = [];
			this.exclusions = [];
		};


		ViewState.prototype = {

			addElement: function ($el, isIncluded) {
				(isIncluded ? this.inclusions : this.exclusions).push(new ViewStateElement($el, this.keepLayout));
			},

			/**
			 * the exit function removes all the state style class on each included target
			 */
			exit: function () {
				_utils._.each(this.inclusions, function (viewStateEl) {
					viewStateEl.removeStateStyle(this.name);
				}, this);
			},

			/**
			 * the enter function hides all exclusions and shows all inclusions
			 */
			enter: function () {
				_utils._.each(this.exclusions, function (viewStateEl) {
					this._update(viewStateEl, false);
				}, this);
				_utils._.each(this.inclusions, function (viewStateEl) {
					this._update(viewStateEl, true);
				}, this);
			},

			/**
			 * update the visibility of a the state element and applies the state style class if the element has to be visible
			 * @param element
			 * @param mustDisplay
			 * @private
			 */
			_update: function (target, mustDisplay) {
				if (mustDisplay !== target.isDisplayed()) target.toggle();
				if (mustDisplay) target.addStateStyle(this.name);
			}

		};

		core.ViewState = ViewState;

	}(_core));


	/**
	 * the ViewStatesManager is the delegate of a StatesClient that manages its state change
	 * @type {*}
	 */
	(function (core) {

		/**
		 *
		 * @param attributes
		 * @param options
		 * @constructor
		 */
		var ViewStatesManager = function (attributes, options) {
			this.client = attributes.client;
			this.states = attributes.states;
			this.initialize(options);
		};

		ViewStatesManager.prototype = {

			initialize: function (options) {
				this.silent = options.silent;
				this.nextState = this.currentState = null;
			},

			notifyClient: function (event, stateName) {
				_debug(event, stateName);
				if (!this.silent) this.client[core.config.triggerFunction].call(this.client, event, stateName);
			},

			setState: function (name) {
				this.ensureState(name);
				this.nextState = this.states[name];
				this.exitCurrent();
				this.enterNext();
			},

			ensureState: function (name) {
				if (this.states[name] === void 0) throw new Error("unknown state:-" + name + "-");
			},

			exitCurrent: function () {
				if (!this.currentState) return;
				if (this.nextState === this.currentState.name) return;
				//this.notifyClient(statify.EXIT, this.currentState.name);
				this.currentState.exit();
				//this.notifyClient(statify.EXITED, this.currentState.name);
			},

			enterNext: function () {
				//this.notifyClient(statify.ENTER, this.nextState.name);
				this.nextState.enter();
				this.currentState = this.nextState;
				this.afterNextEntered();
			},
			afterNextEntered: function () {
				this.nextState = null;
				this.client.currentState = this.currentState;
				//this.notifyClient(statify.CHANGED, this.currentState.name);
			}
		};
		core.ViewStatesManager = ViewStatesManager;


	}(_core));


	/**
	 * States builder function: search for all state targets (child elements) in a DOM element to build its view states.
	 * @type {*}
	 */
	_core.buildViewStates = (function (core) {


		var ViewState = core.ViewState,
			_utils = core.utils;
		/**
		 *  Create an object literal with states names as keys.
		 * @param allStatesNames
		 * @return {Object}
		 * @private
		 */
		var _createStatesObject = function (allStatesNames) {
			allStatesNames = allStatesNames.split(",");

			var statesObject = {},
				name;
			while ((name = allStatesNames.shift())) {
				name = name.trim();
				if (statesObject[name]) throw new Error("the state " + name + " is declared twice");
				statesObject[name] = null;
			}
			return statesObject;

		};


		/**
		 * Add an element to a view state
		 * @param states: a map of all the ViewStates
		 * @param name : the name of the state on which a state target must be added
		 * @param $el : the parent DOM element defined as the state container
		 * @param $child : the element to be added to the view state
		 * @param keepLayout : the layout policy for the child element
		 * @param included : includes the state target in the View State with the "name" name if true, excludes it otherwise
		 * @private
		 */
		var _registerStateElement = function (states, stateName, $el, $child, keepLayout, included) {
			stateName = stateName.trim();
			if (stateName === '') throw new Error("declared state on a child of " + $el.selector + " has an empty name");
			states[stateName] = states[stateName] || new ViewState(stateName, $el, keepLayout);
			states[stateName].addElement($child, included);
		};

		/**
		 *
		 * @param states
		 * @param elStateNames
		 * @param $container
		 * @param $el
		 * @param keepInLayout
		 * @param includeIt
		 */
		var _registerElementInViewStates = function (states, elStateNames, $container, $el, keepInLayout, includeIt) {
			_utils._.each(elStateNames, function (stateName) {
				_registerStateElement(states, stateName, $container, $el, keepInLayout, includeIt);
			});
			_utils._.each(_utils._.difference(_utils._.keys(states), elStateNames), function (stateName) {
				_registerStateElement(states, stateName, $container, $el, keepInLayout, !includeIt);
			});
		};

		/**
		 *  populate all view states from statesContext object
		 * @param states
		 * @param statesContext
		 * @private
		 */
		var _populateStates = function (states, statesContext) {
			var $el,
				declaredStates,
				included,
				$statesContainer = statesContext.container;

			_utils._.each(statesContext.elements, function (elementProperties) {
				$el = elementProperties.$el;
				included = elementProperties.inc !== void 0;
				declaredStates = included ? elementProperties.inc : elementProperties.exc;

				if (declaredStates !== void 0) {
					declaredStates = declaredStates.trim() === "all" ? statesContext.names : declaredStates.trim();
					_registerElementInViewStates(states, declaredStates.split(','), $statesContainer, $el, statesContext.keepLayout, included);
				}

			});
			// if includeRoot is true add the state container as an included element of every state
			if (statesContext.includeRoot) {
				_utils._.each(statesContext.names.split(","), function (name) {
					_registerStateElement(states, name, $statesContainer, $statesContainer, statesContext.keepLayout, true);
				});
			}

		};


		return function (stateContext) {
			var states = _createStatesObject(stateContext.names);
			_populateStates(states, stateContext);
			return states;
		};


	}(_core));


	/**
	 *
	 * @type {*}
	 */
	_core.buildStatesContext = (function (core) {
		var _config = core.config,
			_optionsDefaults = {
				includeRoot: false,
				keepLayout: true,
				deepFetch: false,
				reverseTransitions: false,
				silent: false
			},
			_utils = core.utils;
		/**
		 * return all child elements matching the selector value passed as parameter using a jQuery compatible selector API .
		 * If deepFetch is false it returns only direct children, otherwise returns all matching elements.
		 * @param $el
		 * @param selector
		 * @param deepFetch
		 * @return {*}
		 */
		var _findElements = function ($el, selector, deepFetch) {
			if (deepFetch) return $el.find("[" + selector + "]");
			return $el.children("[" + selector + "]");
		};


		/**
		 * build a statesContext object from DOM states attributes declarations
		 * @param $statesContainer
		 * @param options
		 * @return {Array}
		 * @private
		 */
		var _fromDOM = function ($statesContainer, options) {
			var elements = [],
				key,
				$elements,
				elDef,
				$child,
				deepFetch = options.deepFetch;

			_utils._.each([_config.outAttr, _config.inAttr], function (attribute) {

				$elements = _findElements($statesContainer, attribute, deepFetch);
				key = attribute.replace(_config.stateAttr + "-", "");

				_utils._.each($elements, function (child) {
					$child = _utils.$(child);
					elDef = _utils._.find(elements, function (item) {
						return item.$el === $child;
					}) || {
						$el: $child
					};
					elDef[key] = $child.attr(attribute).trim();
					elements.push(elDef);
				});
			});
			return elements;
		};

		/**
		 * build a statesContext object from options hash
		 * @param $stateContainer
		 * @param stateOptions
		 * @return {Array}
		 * @private
		 */
		var _fromOptions = function ($stateContainer, options) {
			var elementsDescriptors = [],
				elementDescriptor,
				$foundElements,
				$element,
				declaredElements = options.elements;

			_utils._.each(_utils._.keys(declaredElements), function (selector) {
				$foundElements = $stateContainer.find(selector);
				_utils._.each($foundElements, function (element) {
					$element = _utils.$(element);
					elementDescriptor = {
						$el: $element
					};
					_utils._.each(["inc", "exc"], function (prop) {
						if (declaredElements[selector][prop] !== void 0) elementDescriptor[prop] = declaredElements[selector][prop];
					});

					if (declaredElements[selector].exc !== void 0) elementDescriptor.exc = declaredElements[selector].exc;
					elementsDescriptors.push(elementDescriptor);
				});

			});
			return elementsDescriptors;
		};


		return function ($statesContainer, options) {
			if (!$statesContainer || $statesContainer.size === 0) throw new Error("cannot build states on an unknown element");
			var ctx = {},
				findElements;
			// the state names must be declared in DOM or options . Options declaration prevails
			ctx.names = options.names || $statesContainer.attr(_config.stateAttr);
			if (ctx.names === void 0) throw new Error($statesContainer.selector + " has no state declared");

			ctx.container = $statesContainer;
			//default properties initialization
			_utils._.extend(ctx, _optionsDefaults);
			_utils._.extend(ctx, options);

			ctx.initialState = options.initialState ? options.initialState : ctx.names.split(",")[0];

			findElements = _utils._.isObject(options.elements) ? _fromOptions : _fromDOM;
			ctx.elements = findElements($statesContainer, options);
			return ctx;
		};
	})(_core);


	/**
	 * The StateClient defines methods to enhance the client object with view states
	 * - it delegates the creation of its states to the buildStates function
	 * - it delegates the management of the states lifecycle to its stateManager member
	 * - it handles state life-cycle events with the 'onStateEvent' function
	 * @type {*}
	 */
	_core.StatesClient = (function (core) {

		var _buildStateContext = core.buildStatesContext,
			_buildStates = core.buildViewStates,
			ViewStatesManager = core.ViewStatesManager,
			_events = [core.EXIT, core.EXITED, core.ENTER, core.CHANGED],
			_utils = core.utils;


		var initializeStates = function () {
			this.currentState = null;
			this.applyDefaults();
			var statesContext = _buildStateContext(this.$el, this.options);
			var states = _buildStates(statesContext);
			this.manageStates(states);
			this.setState(statesContext.initialState);
		};

		var applyDefaults = function () {
			this.options = this.options || {};
			if (this.statesDefaults !== void 0) {
				_utils._.extend(this.options, this.statesDefaults);
			}
		};

		var manageStates = function (states) {
			this.stateManager = new ViewStatesManager({
				client: this,
				states: states
			}, this.options);
			this.on(_events.join(" "), _utils._.bind(this.onStateEvent, this));
		};


		var setState = function (name) {
			this.stateManager.setState(name);
		};

		/**
		 * this function defined as the state life cycle events handler is intended to be overridden
		 * @param event: the event name for instance state:enter
		 * @param stateName: the state that is targeted by this event
		 */
		var onStateEvent = function (event, stateName) {
			_debug(this.$el.selector + " " + (event.type || event) + " on " + stateName);
		};


		return {
			initializeStates: initializeStates,
			applyDefaults: applyDefaults,
			manageStates: manageStates,
			setState: setState,
			onStateEvent: onStateEvent

		};

	})(_core);


	return _core;


}(this));