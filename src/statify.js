(function (root) {

	"use strict";

	var statify = root.statify,
		setTimeout = root.setTimeout,
		document = root.document,
		CSS = statify.CSS;


	/**
	 * CSS transitions utility functions.
	 * @type {*}
	 */
	(function (statify) {

		var CSS = statify.CSS;
		var utils = statify.utils;

		var _durationsCache = {};

		utils._.extend(CSS, {

			transitions: (function () {
				var el = document.createElement('div'),
					transProps = ['transition', 'OTransition', 'MSTransition', 'MozTransition', 'WebkitTransition'],
					props = el.style,
					prop = utils._.find(transProps, function (value) {
						return props[value] === void 0 ? false : value;
					});

				return prop && {
					durationProp: prop + "Duration",
					delayProp: prop + "Delay"
				};
			}()),

			/**
			 * return the maximum number in milliseconds of a comma separated expression of CSS durations declarations
			 * for instance '1s,2s' returns 2000.
			 * @param expression
			 */
			getMillis: function (expression) {
				var tokens = expression.split(","),
					millis = 0,
					t;
				utils._.each(tokens, function (token) {
					t = parseFloat(token.replace("s", "").replace("m", ""));
					millis = Math.max(millis, isNaN(t) ? 0 : t * (token.indexOf("ms") === -1 ? 1000 : 1));
				});
				return millis;
			},

			/**
			 * return the total duration including delay of all the CC3 transitions in a CCS rule having the selector passed as parameter
			 * @param selector
			 */
			getFullDuration: function (selector) {
				if (!CSS.transitions) return 0;
				return _durationsCache[selector] || (function () {
					if (!CSS.hasRule(selector)) {
						_durationsCache[selector] = 0;
						return 0;
					}
					var style = CSS.findStyle(selector);
					var duration = CSS.getMillis(style[CSS.transitions.durationProp]) + CSS.getMillis(style[CSS.transitions.delayProp]);
					_durationsCache[selector] = duration;
					return duration;
				})();

			}
		});

	}(statify));




	/**
	 * The ViewStateTransition triggers the CSS3 transitions on a view state when a style containing transitions is applyied
	 * It waits for the transitions to be completed to invoke a callback method.
	 * The listening strategy is based on full transition duration and not on 'transitionEnd' event handling.
	 * @type {*}
	 */

	(function (statify) {

		var CSS = statify.CSS;
		var utils = statify.utils;


		var _getTotalDuration = function (state) {
			if (state.transDuration >= 0) return state.transDuration;
			var duration = 0;
			utils._.each(state.inclusions, function (includedElement) {
				duration = Math.max(CSS.getFullDuration(includedElement.getStyleSelector(state.name)), duration);
			});
			state.transDuration = duration;
			return duration;
		};

		var _start = function (state, duration, callBack, reverseIt) {
			if (reverseIt) state.exit();
			else state.enter();
			if (duration === 0) {
				callBack();
				return;
			}
			setTimeout(callBack, duration);
		};



		var ViewStateTransition = function () {
			this.initialize();
		};


		ViewStateTransition.prototype = {

			initialize: function () {
				this.isPlaying = false;
				this.callBack = null;
				this.onComplete = utils._.bind(this._onComplete, this);
			},


			playOn: function (state) {
				this._startOn(state, false);
			},

			reverseOn: function (state) {
				this._startOn(state, true);
			},


			_startOn: function (state, reverseIt) {
				this.isPlaying = true;
				var duration = _getTotalDuration(state);
				_start(state, duration, this.onComplete, reverseIt);
			},

			_onComplete: function () {
				this.callBack();
				this.isPlaying = false;
			}

		};

		statify.ViewStateTransition = ViewStateTransition;

	}(statify));


	/**
	 * the ViewStatesManager is rewritten to manages states and CSS3 transitions on state change
	 * @type {*}
	 */
	(function (statify) {

		var CSS = statify.CSS;
		var utils = statify.utils;
		var ViewStateTransition = statify.ViewStateTransition;
		var ViewStatesManager = statify.ViewStatesManager;



		ViewStatesManager.prototype = utils._.extend(ViewStatesManager.prototype, {

			initialize: function (options) {
				this.reverseTransitions = options.reverseTransitions;
				this.transition = new ViewStateTransition();
				this.onCurrentExited = utils._.bind(this.enterNext, this);
				this.completeCallBack = utils._.bind(this.afterNextEntered, this);
			},

			setState: function (name) {
				if (this.transition.isPlaying) return;
				this.ensureState(name);
				this.nextState = this.states[name];
				if (this.nextState === this.currentState) return;
				this.exitCurrent();
			},

			exitCurrent: function () {
				if (!this.currentState) {
					this.enterNext();
					return;
				}
				this.notifyClient(statify.EXIT, this.currentState.name);
				if (this.reverseTransitions) {
					this.transition.callBack = this.onCurrentExited;
					this.transition.reverseOn(this.currentState);
				} else {
					this.currentState.exit();
					this.enterNext();
				}

			},

			enterNext: function () {
				if (this.currentState) {
					this.notifyClient(statify.EXITED, this.currentState.name);
				}
				this.notifyClient(statify.ENTER, this.nextState.name);
				this.transition.callBack = this.completeCallBack;
				this.transition.playOn(this.nextState);
			},

			afterNextEntered: function () {
				this.currentState = this.nextState;
				this.notifyClient(statify.CHANGED, this.currentState.name);
			}

		});

	}(statify));


}(this));