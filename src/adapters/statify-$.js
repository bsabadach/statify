/* jshint strict: true */
/* global $,statify */
(function (root, $, statify) {

	'use strict';

	//---------------------------------------------------------------------------------
	// utilities
	//---------------------------------------------------------------------------------
	var slice = [].slice,
		concat = [].concat,
		require = root.require,
		_ = root._;
	

	if (!_ && (typeof require !== 'undefined')) _ = require('underscore');
	
	/*
	 * if underscore is not loaded implements _ API used in statify with $ functions adapters
	 */

	_ = _ || {
		isFunction: $.isFunction,

		isArray: $.isArray,

		find: function (list, iterator) {
			var found = $.grep(list, iterator);
			return found[0];
		},

		indexOf: function (list, value) {
			return $.inArray(value, list);
		},

		each: function (list, iterator, ctx) {
			return $.each(list, function (index, value) {
				if (ctx !== void 0) iterator.call(ctx, value, index);
				else iterator(value, index);
			});
		},

		difference: function (array) {
			var rest = concat.apply(slice.call(arguments, 1));
			return $.grep(array, function (value) {
				return !$.inArray(rest, value);
			});
		},

		keys: function (o) {
			return $.map(o, function (value, key) {
				return key;
			});
		},
		isObject: $.isPlainObject,

		extend: $.extend,

		// a brutally simple implementation of bind just for our need in case underscore is not loaded
		bind: function (func, ctx) {
			var memo = func;
			return function () {
				memo.apply(ctx, slice.call(arguments));
			};
		}

	};
	
	
	statify.utils={
		$:$,
		_:_
	};



	$.fn.statify = function (options) {


		return this.each(function () {
			this.$el = this;

			this.options = options || {};

			$.extend(this, statify.StatesClient);

			this.initializeStates();

			return this;
		});

	};



})(this, $, statify);