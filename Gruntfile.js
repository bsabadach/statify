/*global module:false,require:false*/
module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        meta: {
            src_core: 'statify-basic.js',
            src_ext: 'statify-transitions.js',
            src_full: 'statify',
            specs: 'test/*.js',
            vendors: ['test/lib/underscore-1.3.1.js', 'test/lib/jquery-1.7.2.min.js']
        },

        jshint: {
            options: {
                "curly": false,
                "eqnull": true,
                "eqeqeq": true,
                "undef": true
            },
            all: ['src/<%=meta.src_core%>', 'src/<%=meta.src_ext%>']
        },

        concat: {
            main: {
                src: ['src/<%=meta.src_core%>', 'src/<%=meta.src_ext%>'],
                dest: 'src/<%=meta.src_full%>.js'
            }
        },
        uglify: {
            all: {
                files: {
                    'build/statify-basic-<%=pkg.version%>.min.js': ['src/<%=meta.src_core%>'],
                    'build/statify-<%=pkg.version%>.min.js': ['src/<%=meta.src_full%>.js']
                }
            }
        },

        jasmine: {
            spec: {
                src: 'src/*.js',
                options: {
                    vendor: '<%=meta.vendors%>',
                    specs: 'test/statify-specs.js',
                    styles: 'test/statify-specs.css',
                    keepRunner: true
                }
            },
            coverage: {
                src: 'src/*.js',
                options: {
                    vendor: '<%=meta.vendors%>',
                    specs: 'test/statify-specs.js',
                    styles: 'test/statify-specs.css',
                    template: require('grunt-template-jasmine-istanbul'),
                    templateOptions: {
                        coverage: 'coverage/json/coverage.json',
                        report: [
                            {
                                type: 'html',
                                options: {
                                    dir: 'coverage/html'
                                }
                            },
                            {
                                type: 'text-summary'
                            }
                        ]
                    }
                }

            }
        },

        jsbeautifier: {
            files: ["src/<%=meta.src%>", "src/<%=meta.src_ext%>", "test/<%=meta.specs%>"]
        },

        copy: {
            simple: {
                src: 'src/<%=meta.src_core%>',
                dest: 'build/statify-basic-<%=pkg.version%>.js'
            },
            full: {
                src: 'src/<%=meta.src_full%>.js',
                dest: 'build/<%=meta.src_full%>-<%=pkg.version%>.js'
            }
        },

        clean: ["build"]

    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-jsbeautifier');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.registerTask('spec', ['jasmine:spec']);
    grunt.registerTask('build', ['jsbeautifier', 'jshint', 'jasmine', 'concat', 'clean', 'copy', 'uglify']);
    grunt.registerTask('coverage', ['jasmine:coverage']);
    grunt.registerTask('format', 'jsbeautifier');
    grunt.registerTask('default', ['jshint', 'jasmine:spec']);
};